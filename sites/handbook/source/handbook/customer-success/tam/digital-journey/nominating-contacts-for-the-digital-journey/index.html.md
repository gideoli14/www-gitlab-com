---
layout: markdown_page
title: "Nominating Contacts for the Digital Journey"
---

Currently there are a variety of programs avaiable to paying customers based on contact role assignment and a number of conditions that must be met to enter the program.

## Adding a Contact via Gainsight
To add a contact please follow these steps:
1. Navigate to the C360 page 
2. Under Contacts find the one you'd like to enroll
3. Next to their name click on the three dots icon (...) and view details
4. Edit the 'GitLab Role' field
5. Scroll through the picklist and select 'GitLab Admin'
6. Save

## Adding a Contact via Salesforce
To add a contact please follow these steps:
1. Find the relevant contact in Salesforce
1. Edit the 'Role' field 
1. Scroll through the picklist and select 'GitLab Admin' 
1. Save 

For the Commercial market, we will require identifying the GitLab Admins at each Account at the time of Opportunity approval submission. [Read more](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/0627fa32a3506a4eedb4bf10725fec7c2981d3d8/sites/handbook/source/handbook/sales/field-operations/sales-systems/gtm-technical-documentation/gainsight/index.html.md) about this requirement.

## Process Following Contact Role Assignment
1. There are multiple programs in Gainsight that listen on a daily basis for new contacts
1. Below are the campaings and their conditions that must be met:

| Program Name | Condition | 
|-----------------|:-------------|
| Customer <50K ARR Onboarding  | Role = GitLab Admin,  TAM = Null, Territory ≠ PubSec, Sales Segment ≠ Large, Customer Since < 3 days, ARR < 50K | 
| Web Direct Onboarding     | Role = Billing Contact,  Source = Web Direct, Territory ≠ PubSec, Sales Segment ≠ Large, Customer Since < 3 days          | 
| Monthly Customer Newsletter      | Role = GitLab Admin         | 
| CI Digital Enablement (Verify)     |  Completed Onboarding Program            | 
| AE to TAM Introduction   |  Role = Billing Contact,  TAM ≠ Null, Account Owner ≠ Null, Territory ≠ PubSec, Sales Segment ≠ Large, Customer Since < 15 days, ARR > 50K | 

Programs will not send to the same contact more than once. The only exclusion to this is the monthly Customer Newsletter.
